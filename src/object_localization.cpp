/**
 * @file object_localization.cpp
 */
#include <object_localization/object_localization.h>
#include <ros/package.h>
#include <pcl/common/centroid.h>
#include <pcl/common/common.h>
#include <pcl/keypoints/uniform_sampling.h>
#include <std_srvs/Trigger.h>

#include <std_srvs/Empty.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf_conversions/tf_eigen.h>
#include <trac_localization/trac_localization.h>
#include <pcl/filters/random_sample.h>

/**
 * @function ObjectLocalization
 * @brief Constructor
 */
ObjectLocalization::ObjectLocalization(const ros::NodeHandle& nh) :
  nh_(nh),
  mesh_package_("object_localization"),
  points_out_("/object_localization/matched_points"),
  affordance_template_(""),
  affordance_template_object_(""),
  affordance_template_id_(0),
  points_(NULL),
  no_cloud_(true)
{

  object_T_guess_.setIdentity();

  trigger_done_=true;

  // get any params
  if(nh_.hasParam("points_out")) nh_.getParam("points_out", points_out_);
  if(nh_.hasParam("affordance_template")) nh_.getParam("affordance_template", affordance_template_);
  if(nh_.hasParam("affordance_template_object")) nh_.getParam("affordance_template_object", affordance_template_object_);
  if(nh_.hasParam("affordance_template_id")) nh_.getParam("affordance_template_id", affordance_template_id_);


  // Subscribe
  points_topic_ = nh_.subscribe("/kinect/kinect/points",1,
                                &ObjectLocalization::pointCloudCallback, this);

  points_pub_ = nh_.advertise<sensor_msgs::PointCloud2>(points_out_, 1);
  points_filtered_pub_ = nh_.advertise<sensor_msgs::PointCloud2>(points_out_ + "/pre_filter", 1);

  match_srv_ = nh_.advertiseService("match_request",
                                    &ObjectLocalization::processMatch,
                                    this);
  

  set_object_pose_client_ = nh_.serviceClient<affordance_template_msgs::SetObjectPose>("/affordance_template_server/set_object_pose");

  // Load Parts
  pcd_files_[object_localization::MatchRequest::Request::JUICE_BOTTLE] = std::string("models/meshes/juice_bottle.pcd") ;
  part_names_[object_localization::MatchRequest::Request::JUICE_BOTTLE] = std::string("juice_bottle");


  // get the model name to match to
  loadModels();
}



ObjectLocalization::~ObjectLocalization()
{
  set_object_pose_client_.shutdown();
}

/**
 * @function pointCloudCallback
 */
void ObjectLocalization::pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr& msg) {
  points_mutex_.lock();
  points_=msg;
  points_mutex_.unlock();
  no_cloud_=false;
}


/**
 * @function processMatch
 */
bool ObjectLocalization::processMatch(object_localization::MatchRequest::Request& req,
                                      object_localization::MatchRequest::Response& resp) {
  
  points_mutex_.lock();
  if (points_==NULL || points_->height*points_->width == 0) {
    ROS_ERROR_STREAM("[ObjectLocalization::processMatch] No points in pointcloud");
    points_mutex_.unlock();
    return false;
  }
  points_mutex_.unlock();
  
  
  return doMatch(req,resp);
}



/**
 * @function doMatch
 * @brief Main Localization
 */
bool ObjectLocalization::doMatch(object_localization::MatchRequest::Request& req,
                                 object_localization::MatchRequest::Response& resp) {

  ROS_INFO("[doMatch] Start Processing. Sim: %d...", sim);


  // Part Type
  if (req.object_request.affordance_template!="")
    affordance_template_ = req.object_request.affordance_template;

  if (req.object_request.affordance_template_object!="")
    affordance_template_object_ = req.object_request.affordance_template_object;

  affordance_template_id_ = req.object_request.affordance_template_id;


  int part;
  if (affordance_template_object_ == "juice_bottle")
    part =  object_localization::MatchRequest::Request::JUICE_BOTTLE;
  else {
    ROS_ERROR_STREAM("[doMatch] Unknown AT frame: "<<affordance_template_object_);
    return false;
  }


  std::string at;
  if (affordance_template_!="") {
    reference_frame_= affordance_template_ + ":" + std::to_string(affordance_template_id_);
    at = affordance_template_object_ + std::string(":") + std::to_string(affordance_template_id_);
  }
  else {
    ROS_ERROR("Affordance template name is EMPTY!!!!");
    return false;
  }

  ROS_WARN("[DEBUG] doMatch: Reference frame: %s  -- Affordance Template: %s ",
           reference_frame_.c_str(), at.c_str() );

  tf::StampedTransform reference_part, original_guess;
  geometry_msgs::Pose part_transform, part_transform_original;


  std::string err_msg;
  
  // Convert Photoneo msg to pointcloud
  ros::Time headerstamp = ros::Time::now();
  points_mutex_.lock();
  std::string headerframe = points_->header.frame_id;
  points_mutex_.unlock();


  try {
    ros::Time tf_time = headerstamp;

    while (!tf_.waitForTransform( reference_frame_, at,
                                  tf_time,
                                  ros::Duration(2.0), ros::Duration(0.01),
                                  &err_msg)) {
      ROS_ERROR_STREAM("Localization::defaultLoc(): No transform from "
                       << reference_frame_<<" to "<<at<<".  Error: "<<err_msg);
      err_msg="";
      tf_time = ros::Time::now();
    }
    tf_.lookupTransform (reference_frame_, at, ros::Time(0), reference_part);

  } catch (...) {
    ROS_ERROR_STREAM("Localization::defaultLoc(): problem finding reference to part transform");
    return false;
  }


  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered2(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered3(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr part_filtered(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_downsampled(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr height_filtered(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr height_filtered2(new pcl::PointCloud<pcl::PointXYZ>);
  model_.reset(new pcl::PointCloud<pcl::PointXYZ>);
  sensor_msgs::PointCloud2 pc2,pc_filtered;


  // transform cloud into search frame
  try {
    ros::Time tf_time = headerstamp;

    while (!tf_.waitForTransform( reference_frame_,
                                  headerframe,
                                  tf_time, ros::Duration(2.0),
                                  ros::Duration(0.01), &err_msg) ) {
      ROS_ERROR_STREAM("Localization::defaultLoc(): No transform from "
                       <<reference_frame_<<" to "<<headerframe<<".  Error: "<<err_msg);
      err_msg="";
      tf_time = ros::Time::now();
    }

    tf::StampedTransform loc2ref_tf;
    tf_.lookupTransform (reference_frame_, headerframe, ros::Time(0), loc2ref_tf);

    geometry_msgs::TransformStamped tfmsg;
    tf::transformStampedTFToMsg(loc2ref_tf,tfmsg);

    points_mutex_.lock();
    tf2::doTransform (*points_, pc2, tfmsg);
    points_mutex_.unlock();

    pcl::fromROSMsg(pc2, *cloud_in);

  } catch (...) {
    ROS_ERROR_STREAM("Localization::defaultLoc(): Problem transforming PC" );
    return false;
  }

  original_guess = reference_part;
  tf::poseTFToMsg(reference_part, part_transform);
  part_transform_original=part_transform;

  ROS_INFO_STREAM("** Guess: "<<part_transform);

  if (cloud_in->points.size() < 10) {
    resp.object_response.score=-1;
    ROS_WARN_STREAM("Localization::defaultLoc(): Not enough points to continue Localization");
    return true;
  }

  geometry_msgs::PoseStamped model_pose;

  pass_.setNegative(false);

  pass_.setInputCloud (cloud_in);
  pass_.setFilterFieldName ("z");
  pass_.setFilterLimits (0.01, 0.25);
  pass_.filter (*cloud_filtered);
  
  pass_.setInputCloud (cloud_filtered);
  pass_.setFilterFieldName("x");
  pass_.setFilterLimits (0.2, 1.0);
  pass_.filter (*cloud_filtered2);
  
  pass_.setInputCloud (cloud_filtered2);
  pass_.setFilterFieldName("y");
  pass_.setFilterLimits (-0.77, 0.77);
  pass_.filter (*cloud_filtered3);
  
  // Downsize

  // Create the filtering object
  pcl::UniformSampling< pcl::PointXYZ > sor;
  sor.setInputCloud (cloud_filtered3);
  sor.setRadiusSearch(0.005);
  pcl::PointCloud<int> keypointIndices1;
  sor.compute(keypointIndices1);
  pcl::copyPointCloud(*cloud_filtered3, keypointIndices1.points, *part_filtered);


  double downsample_param=100000;
  if (part_filtered->points.size() > downsample_param) {
    pcl::RandomSample<pcl::PointXYZ> random_sampler;
    random_sampler.setInputCloud(part_filtered);
    random_sampler.setSample(downsample_param);
    random_sampler.filter(*cloud_filtered3);
    part_filtered=cloud_filtered3;
  }

  //  pcl::copyPointCloud(*cloud_in,*part_filtered);
  ROS_INFO("* Part filtered size: %d ", (int)part_filtered->points.size() );


  //cloud_filtered
  pcl::toROSMsg(*part_filtered, pc_filtered);
  pc_filtered.header.frame_id = reference_frame_;
  points_filtered_pub_.publish(pc_filtered);

  tf::Transform tf;
  // Get bounds

  pcl::copyPointCloud(*pcd_models_[part],*model_);
  
  int score;
  TRAC_Localization localizer;
  ROS_WARN("Starting run_localization......");
  
  score = localizer.solve(model_,
                          part_filtered,
                          original_guess,
                          tf,
                          0.01,
                          10);
  if( score < 0 ) {
    ROS_WARN("** Score less than 0");

    resp.object_response.score = -1;
    return true;
  }
  bool exit_loop; bool status;
  publish_object_param_results( tf, part,
                                score,
                                part_transform_original,
                                req, resp, exit_loop, status );
  if( exit_loop ) { return status; }

  return true;
}




/**
 * @function publish_object_param_results
 */
void ObjectLocalization::publish_object_param_results( tf::Transform _tf,
                                                       int _part,
                                                       int score,
                                                       geometry_msgs::Pose _part_transform_original,
                                                       object_localization::MatchRequest::Request& _req,
                                                       object_localization::MatchRequest::Response& _resp,
                                                       bool &_exit, bool &_status ) {
  double score1, score2;
  Eigen::Matrix4d tmp;
  Eigen::Affine3d etf;
  Eigen::Matrix4f guess;
  pcl::PointCloud<pcl::PointXYZ>::Ptr matched_points(new pcl::PointCloud<pcl::PointXYZ>);

  tf::transformTFToEigen( _tf, etf);

  pcl::transformPointCloud(*model_,*matched_points, etf );

  score1 = score/100.0;
  ROS_INFO_STREAM("Final match score "<<score1);

  nh_.setParam("/match_score", score1);

  _resp.object_response.score = score1;

  Eigen::Affine3f object_T_=etf.cast<float>();
  tf::poseTFToMsg( _tf, object_pose_ );

  geometry_msgs::PoseStamped ps;
  ps.pose = object_pose_;
  ps.header.frame_id = reference_frame_;
  ps.header.stamp = ros::Time::now();
  std::string frame= reference_frame_;

  ROS_INFO_STREAM("Part location: "<<ps.pose);
  _resp.object_response.part_location = ps.pose;

  tf::poseMsgToEigen(_part_transform_original, object_T_guess_);
  guess = object_T_guess_.cast<float>().matrix();

  guess = object_T_.inverse()*guess;
  tmp = guess.cast<double>();
  Eigen::Affine3d guess_aff(tmp);

  tf::transformEigenToMsg(guess_aff,_resp.object_response.guess_error);

  // publish out stuff for visualization of correspondance
  sensor_msgs::PointCloud2 matched_cloud;
  pcl::toROSMsg(*matched_points, matched_cloud);
  matched_cloud.header.frame_id = reference_frame_;
  points_pub_.publish(matched_cloud);

  _resp.object_response.pose_success = true;

  double r,p,y;
  tf::Transform T;

  tf::transformEigenToTF(guess_aff,T);
  T.getBasis().getRPY(r,p,y);

  nh_.setParam("/object_part_guess_error/x", T.getOrigin().x());
  nh_.setParam("/object_part_guess_error/y", T.getOrigin().y());
  nh_.setParam("/object_part_guess_error/z", T.getOrigin().z());
  nh_.setParam("/object_part_guess_error/R", r);
  nh_.setParam("/object_part_guess_error/P", p);
  nh_.setParam("/object_part_guess_error/Y", y);

  if (_req.object_request.allowable_error.size() == 2)
    if (std::abs(T.getOrigin().x()) > _req.object_request.allowable_error[0].x ||
        std::abs(T.getOrigin().y()) > _req.object_request.allowable_error[0].y ||
        std::abs(T.getOrigin().z()) > _req.object_request.allowable_error[0].z ||
        std::abs(r) > _req.object_request.allowable_error[1].x ||
        std::abs(p) > _req.object_request.allowable_error[1].y ||
        std::abs(y) > _req.object_request.allowable_error[1].z)
      _resp.object_response.pose_success = false;

  if (_resp.object_response.score < _req.object_request.score_threshold) {
    _resp.object_response.match_success = false;

    ROS_ERROR_STREAM("Bad match score of "<<_resp.object_response.score);
    _exit = true;
    _status = true;
    return;
  }

  ROS_INFO_STREAM("Localization: Match is better than "<<_req.object_request.score_threshold);
  _resp.object_response.match_success = true;


  if (affordance_template_!="")
    if (_resp.object_response.pose_success && _resp.object_response.match_success) {
      ROS_WARN("ObjectLocalization -- setting part AT");
      if(!setAffordanceTemplateObjectPose(affordance_template_,
                                          affordance_template_id_,
                                          affordance_template_object_, ps)) {
        ROS_ERROR("ObjectLocalization -- problem setting part");
        _exit = true;
        _status = false;
        return;
      }
    }

  _exit = false;
  _status = true;
}

/**
 * @function spin
 * @brief
 */
void ObjectLocalization::spin()
{

  ros::Rate loop_rate(10);
  tf::Transform transform;

  ROS_INFO("%s started.", nh_.getNamespace().c_str());

  ros::AsyncSpinner spinner(2);
  spinner.start();

  while(ros::ok())
    {
      loop_rate.sleep();
    }

  ros::waitForShutdown();

}


bool ObjectLocalization::setAffordanceTemplateObjectPose(std::string template_name, int id, std::string object_name, geometry_msgs::PoseStamped ps)
{

  affordance_template_msgs::SetObjectPose srv;
  affordance_template_msgs::DisplayObjectInfo obj;

  obj.type = template_name;
  obj.name = object_name;
  obj.id = id;
  obj.stamped_pose = ps;
  srv.request.objects.push_back(obj);

  if (set_object_pose_client_.call(srv)) {
    ROS_INFO("set object pose successful");
  } else {
    ROS_ERROR("failed to set object pose");
    return false;
  }

  return true;
}

/**
 * @function loadModels
 * @brief Load .stl and .pcd files
 */
bool ObjectLocalization::loadModels()
{
  for( unsigned int i = 0; i < object_localization::MatchRequest::Request::NUM_PARTS; ++i ) {

    pcd_models_[i].reset ( new pcl::PointCloud<pcl::PointXYZ> );
    std::string root = ros::package::getPath(mesh_package_);
    root= root+"/"+pcd_files_[i];

    //* load the file
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (root, *pcd_models_[i]) == -1) {
      ROS_ERROR_STREAM("Couldn't read PCD file " << root);
      return false;
    }
    ROS_INFO_STREAM("Loaded " << pcd_models_[i]->width * pcd_models_[i]->height << " data points from "  << pcd_files_[i]);

  } // for all parts

  return true;

}



/**********/
/** MAIN  */
/**********/
int main(int argc, char **argv)
{

  // init ros
  ros::init(argc, argv, "object_localization");
  ros::NodeHandle nh("~");

  // create new PL object
  ObjectLocalization pl(nh);
  ROS_INFO("Start to spin..");
  // start spinning
  pl.spin();

  return 0;
}

