

#include <object_localization/SeedRequest.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf/transform_datatypes.h>
#include <object_localization/find_objects.h>
#include <ros/package.h>

#include <ros/ros.h>

class ObjectLocalization {

public:
  ObjectLocalization( ros::NodeHandle _nh ) {
    nh_ = _nh;
    find_objects_.reset( new FindObjects( nh_, 2064, 1544) );
    seed_srv_ = nh_.advertiseService("seed_request",
				     &ObjectLocalization::seedRequest,
				     this );
    
  }

  /**
   * @function seedRequest
   */
  bool seedRequest( object_localization::SeedRequest::Request& _req,
		    object_localization::SeedRequest::Response& _res ) {
    /*
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::fromROSMsg( _req.cloud, cloud );
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ptr = boost::make_shared< pcl::PointCloud<pcl::PointXYZ> >( cloud );
  
    ROS_WARN("Seed Request start...");
    cv::Mat objects_on_table_gray;
    //fo.set_table_coeffs( -0.143164, -0.005653, -0.989683, 0.906020 );
    find_objects_->set_table_filters( 0.2, 1.0,  
				     -0.8, 0.8,
				      -0.10, 0.20 );
    bool do_table_segmentation = !( find_objects_->is_part_tableCoeffs_set() ); 
    if( !find_objects_->get_depth_info( cloud_ptr,
					objects_on_table_gray,
					do_table_segmentation ) ) {
      ROS_WARN("Error getting objects on table gray image");
      _res.success = false;
      return true;
    }
    
    // Find likely locations
    std::vector<Detected_Object> objs;
    cv::Mat debug_img;
    bool filter_out_shadows = false;
    debug_img = find_objects_->find_likely_locations( objects_on_table_gray,
						      objs, filter_out_shadows );
    
    std::string image_filename;
    std::string pkg_path = ros::package::getPath("dortek_demo");
    image_filename = pkg_path + "/test_display.png";
    
    cv::imwrite( image_filename, debug_img );

    geometry_msgs::PoseStamped pose_housing;
    geometry_msgs::PoseStamped pose_part;

    if( objs.size() < 2 ) {
      ROS_WARN("Objects found in image are too few: %d ", (int)objs.size() );
      _res.success = false;
    }
    pose_housing.header = _req.cloud.header;
    pose_housing.pose.position.x = objs[0].x;
    pose_housing.pose.position.y = objs[0].y;
    pose_housing.pose.position.z = objs[0].z;
    pose_housing.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw( objs[0].roll, objs[0].pitch, objs[0].yaw );
    
    pose_part.header = _req.cloud.header;
    pose_part.pose.position.x = objs[1].x;
    pose_part.pose.position.y = objs[1].y;
    pose_part.pose.position.z = objs[1].z;
    pose_part.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw( objs[1].roll, objs[1].pitch, objs[1].yaw );

    _res.success = true;
    _res.pose_housing = pose_housing;
    _res.pose_part = pose_part;
    */
    ROS_WARN("Test localization callback end");

    return true;
  }

  
protected:
  ros::ServiceServer seed_srv_;
  ros::NodeHandle nh_;
  std::shared_ptr<FindObjects> find_objects_;
};

int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "test_service_seed" );
  ros::NodeHandle nh;

  ObjectLocalization qt(nh);

  ros::Rate r(10);
  while( ros::ok() ) {
    r.sleep();
    ros::spinOnce();
  }

  return 0;
  
}
