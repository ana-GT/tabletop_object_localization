#include <ros/ros.h>
#include <ros/package.h>
#include <std_srvs/Trigger.h>

#include <phoxi_camera/ConnectCamera.h>
#include <phoxi_camera/TriggerImage.h>
#include <phoxi_camera/GetFrame.h>
#include <phoxi_camera/IsConnected.h>
#include <phoxi_camera/IsAcquiring.h>
#include <std_srvs/Empty.h>

#include <tf_conversions/tf_eigen.h>

#include <tf/transform_listener.h>
#include <mutex>

//#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>

/**
 * @class StoreImageCloud
 * @brief Constructor
 */
class StoreImageCloud {

public:
 /** Constructor */
 StoreImageCloud(const ros::NodeHandle& nh) :
   nh_(nh) {

   trigger_done_=true;
   std::string node_name = ros::this_node::getName();
   photoneo_serial_number_ = "2018-03-025-LC3"; // Housing
   if (nh_.getParam( node_name + "photoneo_serial_number",photoneo_serial_number_))
     ROS_INFO("Using user supplied serial");

   ROS_INFO_STREAM("Using serial number: "<<photoneo_serial_number_);
  
   while (!start_config_photoneo()){
     ROS_ERROR_THROTTLE(2, "Waiting on Photoneo software to start.");
     ros::Duration(1).sleep();
   }
  
    points_sub_ = nh_.serviceClient<phoxi_camera::GetFrame>("phoxi_camera/get_frame");
    points_sub2_ = nh_.serviceClient<phoxi_camera::TriggerImage>("phoxi_camera/trigger_image");

    // Offer service to store pointcloud and image
    store_srv_ = nh_.advertiseService( node_name + "/store_image_cloud",
				       &StoreImageCloud::store_srv_cb, this );
    index_ = 0;
 }  

  /** @function start_config_photoneo */
  bool start_config_photoneo() {
    
    // 1. Call service to connect to Photoneo
    phoxi_camera::ConnectCamera srv_conn; 
    ros::ServiceClient client_conn = nh_.serviceClient<phoxi_camera::ConnectCamera>("phoxi_camera/connect_camera" );
    srv_conn.request.name = photoneo_serial_number_;
    do {
      ROS_WARN_THROTTLE(1,"Connect with Photoneo. Serial number: %s ", photoneo_serial_number_.c_str());
      if( !client_conn.call( srv_conn ) ) {
	ROS_ERROR("[StoreImageCloud::start_config_photoneo] Failed service call to Photoneo");
	return false;
      }
      if (srv_conn.response.success) 
	break;
    } while (true);
    
    bool is_connected = false;
    ros::ServiceClient client_conn_check = nh_.serviceClient<phoxi_camera::IsConnected>("phoxi_camera/is_connected" );
    phoxi_camera::IsConnected srv_check; 
    while (!is_connected) {
      ROS_WARN_STREAM_THROTTLE(1,"Waiting to connect to "<<photoneo_serial_number_);
      if( !client_conn_check.call( srv_check ) ) {
	ROS_ERROR("[StoreImageCloud::start_config_photoneo] Failed connecting to Photoneo");
	return false;
      } 
      is_connected=srv_check.response.connected;
    }
    
    
    // 2. Call service to start acquisition
    std_srvs::Empty srv_start;
    ros::ServiceClient client_start = nh_.serviceClient<std_srvs::Empty>("phoxi_camera/start_acquisition" );
    
    if( !client_start.call( srv_start ) ) {
      ROS_ERROR("[StoreImageCloud::start_config_photoneo] Failure starting acquisition");
      return false;
    } 
    
    
    bool is_acquiring = false;
    ros::ServiceClient client_acq_check = nh_.serviceClient<phoxi_camera::IsAcquiring>("phoxi_camera/is_acquiring");
    phoxi_camera::IsAcquiring srv_acq; 
    while (!is_acquiring) {
      ROS_WARN_STREAM_THROTTLE(1,"Waiting for acquiring");
      if( !client_acq_check.call( srv_acq ) ) {
	ROS_ERROR("[StoreImageCloud::start_config_photoneo] Failed acquiring Photoneo");
	return false;
      } 
      ROS_WARN("Seems it is good to go! phoxi is acquiring!");
      is_acquiring=srv_acq.response.is_acquiring;
    }
    
    
    return true;
  } // end 

  /** Service offered */
  bool store_srv_cb( std_srvs::Trigger::Request  &req,
		     std_srvs::Trigger::Response &res ) {
    ROS_WARN("Received service request to store image and cloud!");
    ROS_WARN("Calling getPhoxiCloud");
    bool b = triggerAndGetCloud();
    if( !b ) { ROS_WARN("Return false store_srv_cb"); return false; }
    ROS_WARN("(a) Storing image and cloud");

    cv_bridge::CvImagePtr cv_ptr;
    ROS_WARN("Get image");
    try{
      cv_ptr = cv_bridge::toCvCopy( image_, sensor_msgs::image_encodings::TYPE_32FC1);
    } catch (cv_bridge::Exception& e) {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return false;
    }
    // Store image
    std::string image_filename;
    std::string pkg_path = ros::package::getPath("dortek_demo");
    image_filename = pkg_path + "/image_" + std::to_string(index_) +".png";
    ROS_WARN("(b) Storing image in %s", image_filename.c_str() );
    imwrite( image_filename, cv_ptr->image );

    // Store pointcloud
    std::string cloud_filename = pkg_path + "/pointcloud_" + std::to_string(index_) + ".pcd";
    ROS_WARN("(c) Store pointcloud in %s", cloud_filename.c_str() );
    pcl::PointCloud<pcl::PointXYZRGBA> cloud;
    pcl::fromROSMsg( *points_, cloud );
    // Save cloud
    ROS_WARN("(d) Save cloud ASCII ");
    pcl::io::savePCDFileASCII( cloud_filename, cloud );
    res.success = b;
    index_++;
    return true;
  }
  
  
  bool triggerPhoxi() {

    // Trigger 
    phoxi_camera::TriggerImage srv_trigger;
    ROS_INFO("Calling triggering service...");
    if( !points_sub2_.call( srv_trigger ) || !srv_trigger.response.success) {
      ROS_ERROR("[StoreImageCloud::triggerPhoxi] Failure triggering");
      return false;
    } else {
      ROS_INFO("[StoreImageCloud::triggerPhoxi] Soft Trigger was successful");
      return true;
    }
    
  }
  
  bool triggerAndGetCloud() {
    bool b1 = triggerPhoxi();
    ros::spinOnce(); usleep(0.1*1e6);
    bool b2 = triggerPhoxi();
    ros::spinOnce(); usleep(0.1*1e6);
    bool b = b1 || b2;
    if( b ) { return getPhoxiCloud(); }
    else { ROS_WARN("Error in triggerPhoxi"); return false; }
  }
  
  
  /** getPhoxiCloud */
  bool getPhoxiCloud() {

    phoxi_camera::GetFrame srv_get_frame;
    srv_get_frame.request.in = 0;
    ros::ServiceClient client_get_frame = nh_.serviceClient<phoxi_camera::GetFrame>("phoxi_camera/get_frame" );

    bool got_image = false;
    ros::Time start_time = ros::Time::now();
    while (ros::Time::now()-start_time < ros::Duration(10.0)) {
      ros::spinOnce();
      if( !client_get_frame.call( srv_get_frame ) ) {
	ROS_ERROR("[StoreImageCloud::getPhoxiCloud] Failure getting frame");
	return false;
      }
      if (!srv_get_frame.response.success) {
        ROS_WARN_THROTTLE(1,"[StoreImageCloud::getPhoxiCloud] Frame not ready");
        ros::spinOnce();
        usleep(1.0*1e6);
      } else  {
        ROS_INFO("[StoreImageCloud::getPhoxiCloud] Getting frame was successful");
        points_ = boost::make_shared<sensor_msgs::PointCloud2>(srv_get_frame.response.cloud);
        image_ = boost::make_shared<sensor_msgs::Image>( srv_get_frame.response.img );
        ROS_WARN("Point cloud size: %d %d image: %d %d ", points_->height, points_->width, image_->height, image_->width );

        return true;
      }
    } // while

  
  return false;
}

  
  // ros params
  ros::NodeHandle nh_;
  tf::TransformListener tf_;

  std::mutex points_mutex_;

 
  // storage of latest point cloud
  sensor_msgs::PointCloud2ConstPtr points_;
  sensor_msgs::ImageConstPtr image_;
  
  // ros pub/subs
  ros::Subscriber points_topic_;
  ros::ServiceClient points_sub_, points_sub2_, points_sub3_;
  ros::Publisher points_pub_;
  ros::ServiceServer trigger_srv1_,trigger_srv2_;
  
  // Photoneo
  std::string photoneo_serial_number_;
  double trigger_and_get_wait_time_;

  bool no_cloud_;
  std::string reference_frame_;

  bool trigger_desired_;
  bool trigger_done_;

  // Service
  ros::ServiceServer store_srv_;
  unsigned int index_;
};


/****/
int main( int argc, char* argv[] ) {
 
  // Start node
  ros::init( argc, argv, "store_image_cloud" );
  ros::NodeHandle nh;

  // Offer service to store pictures and cloud
  StoreImageCloud sic(nh);
  ROS_INFO("Node initialized fine. Start to spin..");

  // start spinning
  ros::Rate r(10);
  while( ros::ok() ) {
    ros::spinOnce();
    r.sleep();
  }

  return 0;

}


