#include <iostream>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <ros/package.h>
#include <ros/ros.h>

// This is the main function
int main (int argc, char** argv) {

  ros::init( argc, argv, "rotate_pointcloud" );
  ros::NodeHandle nh;

  std::string pcd_name;
  pcd_name = ros::package::getPath("dortek_demo") + std::string("/models/meshes/switch_cam_rotated.pcd");

  // Load file
  pcl::PointCloud<pcl::PointXYZ>::Ptr source_cloud (new pcl::PointCloud<pcl::PointXYZ> ());

  if (pcl::io::loadPCDFile( pcd_name.c_str() , *source_cloud) < 0 )  {
      printf("Error loading point cloud %s \n", pcd_name.c_str() );
      return 1;
  }


  // Transform
  Eigen::Affine3f Tf = Eigen::Affine3f::Identity();
 
  // The same rotation matrix as before; theta radians around Z axis
  double theta = 38*3.1416/180.0;
  Tf.rotate (Eigen::AngleAxisf (theta, Eigen::Vector3f::UnitY()));

  // Executing the transformation
  ROS_WARN("Rotating pointcloud...");
  pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud (new pcl::PointCloud<pcl::PointXYZ> ());
  // You can either apply transform_1 or transform_2; they are the same
  pcl::transformPointCloud (*source_cloud, *transformed_cloud, Tf );

  // Store
  std::string pcd_name_rotated;
  pcd_name_rotated = ros::package::getPath("dortek_demo") + std::string("/models/meshes/switch_cam_rotated_back.pcd");
  pcl::io::savePCDFileASCII( pcd_name_rotated, *transformed_cloud );
  ROS_WARN("Pointcloud rotated and stored. Good ");

  return 0;
}
