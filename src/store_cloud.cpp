#include <ros/ros.h>
#include <ros/package.h>
#include <pcl_conversions/pcl_conversions.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>

int gCounter_part = 0;
int gCounter_housing = 0;

/**
 * @function part_sub
 */
void part_sub( const sensor_msgs::PointCloud2ConstPtr &_msg ) {
  ROS_WARN("Received pointcloud part %d. Storing it!", gCounter_part );
  pcl::PCLPointCloud2 pc2;
  pcl_conversions::toPCL( *_msg, pc2);

  pcl::PointCloud<pcl::PointXYZ> cloud;
  pcl::fromPCLPointCloud2( pc2, cloud );

  std::string pkg_path = ros::package::getPath("dortek_demo");
  std::string name = pkg_path + "/data/" + "part_" + std::to_string(gCounter_part)  + ".pcd"; 
  pcl::io::savePCDFileASCII ( name, cloud );
  gCounter_part++;
}

/**
 * @function housing_sub
 */
void housing_sub( const sensor_msgs::PointCloud2ConstPtr &_msg ) {
  ROS_WARN("Received pointcloud housing %d. Storing it!", gCounter_housing );
  pcl::PCLPointCloud2 pc2;
  pcl_conversions::toPCL( *_msg, pc2);

  pcl::PointCloud<pcl::PointXYZ> cloud;
  pcl::fromPCLPointCloud2( pc2, cloud );

  std::string pkg_path = ros::package::getPath("dortek_demo");
  std::string name = pkg_path + "/data/" + "housing_" + std::to_string(gCounter_housing)  + ".pcd"; 

  pcl::io::savePCDFileASCII ( name, cloud );
  gCounter_housing++;
}



/**
 *
 */
void texture_sub( const sensor_msgs::ImageConstPtr & _msg ) {
 ROS_WARN("-- Encoding: %s ", _msg->encoding.c_str() );
 cv_bridge::CvImagePtr img_ptr;
 img_ptr = cv_bridge::toCvCopy( *_msg, _msg->encoding );
 cv::Mat texture_float = img_ptr->image;

  // Go from 32FC1 to BGR8
  double min_z, max_z;
  cv::Mat texture_view;
  minMaxLoc( texture_float, &min_z, &max_z ); max_z = 900.0;
  ROS_WARN("Min/max z: %f / %f ", min_z, max_z );
  texture_float.convertTo( texture_view, CV_8U,
		    255.0/(max_z - min_z),
		    -min_z*255/(max_z - min_z) );
  ROS_WARN("Store bag img ");
  imwrite("bag_img.png", texture_view );
}

/**
 * @function main
 */
int main( int argc, char* argv[] ) {

  ros::init( argc, argv, "store_cloud" );
  ros::NodeHandle nh;
  // Store a pointcloud
  ros::Subscriber sub_part = nh.subscribe("/part/phoxi_camera/pointcloud", 10, part_sub );
  ros::Subscriber sub_housing = nh.subscribe("/housing/phoxi_camera/pointcloud", 10, housing_sub );

  // Store texture
  ros::Subscriber sub_text = nh.subscribe("/phoxi_camera/texture", 10, texture_sub );
  
  ros::Rate r(30);
  while( ros::ok() ) {
    ros::spinOnce();
    r.sleep();
  }
}
