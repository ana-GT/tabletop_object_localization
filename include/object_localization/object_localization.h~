#ifndef PART_LOCALIZATION__H_
#define PART_LOCALIZATION__H_

#include <algorithm>  
#include <ros/ros.h>
#include <sstream>
#include <thread>

#define BOOST_NO_CXX11_SCOPED_ENUMS
#include <boost/filesystem.hpp>

#include <mutex>

#include <trac_localization/trac_localization.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/transforms.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/JointState.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/point_cloud_conversion.h>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <visualization_msgs/Marker.h>
#include <affordance_template_msgs/SetObjectPose.h>

#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>

#include <object_localization/MatchRequest.h>
#include <object_localization/LocalizeRequest.h>
#include <object_localization/LocalizeResponse.h>
#include <object_localization/TriggerRequest.h>
#include <object_localization/TriggerResponse.h>

//#include <Eigen/SVD>
#include <eigen_conversions/eigen_msg.h>
#include <kdl/frames.hpp>



class ObjectLocalization {

public:

  ObjectLocalization(const ros::NodeHandle& nh);
  ~ObjectLocalization();
  // spin function
  void spin();


protected:

  bool start_config_photoneo(); 
  bool triggerAndGetCloud();
  bool triggerPhoxi();
  bool getPhoxiCloud();
  bool loadModels();
  // callbacks  
  bool processMatch(object_localization::MatchRequest::Request&, object_localization::MatchRequest::Response&);
  bool doMatch(object_localization::MatchRequest::Request&, object_localization::MatchRequest::Response&);

  bool triggerLocalization(object_localization::TriggerRequest::Request& req, object_localization::TriggerRequest::Response& resp);

  bool getLocalizationResults(object_localization::TriggerResponse::Request& req, object_localization::TriggerResponse::Response& resp);

  bool estimateAndLocalizePart(object_localization::MatchRequest::Request& req, object_localization::MatchRequest::Response& resp );
  
  bool setAffordanceTemplateObjectPose(std::string template_name, int id, std::string object_name, geometry_msgs::PoseStamped ps);

  visualization_msgs::Marker makeModelMarker(geometry_msgs::PoseStamped ps,
                                             unsigned int _part = 0, // HOUSING
                                             int color=0 );
  
  void publish_object_param_results( tf::Transform _tf,
                                     int _part,
                                     int score,
                                     geometry_msgs::Pose _part_transform_original,
                                     object_localization::MatchRequest::Request& _req,
                                     object_localization::MatchRequest::Response& _resp,
                                     bool &_exit, bool &_status );
  
  // ros params
  ros::NodeHandle nh_;
  tf::TransformListener tf_;

  std::mutex points_mutex_;

 
  // storage of latest point cloud
  sensor_msgs::PointCloud2ConstPtr points_;

  std::string points_in_;
  std::string points_out_;
  std::string mesh_package_;
  
  std::string pcd_files_[object_localization::MatchRequest::Request::NUM_PARTS];
  std::string part_names_[object_localization::MatchRequest::Request::NUM_PARTS];
  // affordance template stuff
  std::string affordance_template_;
  std::string affordance_template_object_;
  int affordance_template_id_;

  
  // ros pub/subs
  ros::Subscriber points_topic_;
  ros::ServiceClient points_sub_, points_sub2_, points_sub3_;
  ros::Publisher points_pub_;
  ros::Publisher points_filtered_pub_;
  ros::Publisher marker_pub0_, marker_pub1_, marker_pub2_;
  ros::ServiceServer match_srv_;
  ros::ServiceServer simple_match_srv_;
  ros::ServiceServer trigger_srv1_,trigger_srv2_;
  ros::ServiceServer estimate_and_localize_srv_;
  ros::ServiceClient set_object_pose_client_;

  
  // localization stuff
  pcl::PassThrough<pcl::PointXYZ> pass_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr pcd_models_[object_localization::MatchRequest::Request::NUM_PARTS]; 
  
  Eigen::Matrix4f object_T_;
  geometry_msgs::Pose object_pose_;
  Eigen::Affine3d object_T_guess_;

  // Photoneo
  std::string photoneo_serial_number_;
  double trigger_and_get_wait_time_;

  bool no_cloud_;
  
  bool sim;

  bool getPC();

  std::string reference_frame_;

  pcl::PointCloud<pcl::PointXYZ>::Ptr model_;
  
  void pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr& msg);

  bool trigger_desired_;
  bool trigger_done_;

  object_localization::MatchRequest::Request desired_req_;
  object_localization::MatchRequest::Response desired_resp_;


  int object_iterations_;
  double object_fitness_epsilon_;
  double object_transformation_epsilon_;
  double object_max_correspondence_distance_;
  ros::Subscriber jnt_sub_;
  sensor_msgs::JointState joint_state_;
  std::mutex mtx_;

  double best_min;
  std::vector<double> best_x;
  ros::NodeHandle n_pub_;

    
};


#endif // PART_LOCALIZATION__H_
